@extends('layout.app')

@section('title')
    Taman Bandung
@endsection

@section('description')

@endsection

@section('content')
    <div id="map-canvas"></div>
    <div class="row">
        <div class="search-container col-md-4 col-xs-12">
            <div class="form-horizontal">
                <div class="form-group">
                    <div class="col-md-12">
                        <input type="text" class="form-control search-input quicksearch" id="email" placeholder="Cari Taman Tematik">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="result-container col-md-4 col-xs-12">
            <div class="result-search">
                @foreach($places as $place)
                    @if($place['categories'] == 'Taman' && $place['types'] == 'Taman Tematik')
                        <a href="{{ url('details/' . $place['id']) }}" class="results-link">
                            <div class="row results">
                                <div class="col-md-2">
                                    <img class="img-circle thumbnail-image" src="{{ env('IMG_URL') . $place['thumb'] }}" alt="">
                                </div>
                                <div class="col-md-8">
                                    <h2>{{ $place['name'] }}</h2>
                                    <p>{{ $place['location'] }}</p>
                                </div>
                                <div class="col-md-2">
                                    <img src="{{ url('img/arrow-more.png') }}" alt="" class="arrow-next">
                                </div>
                            </div>
                        </a>
                    @endif
                @endforeach
            </div>
        </div>
    </div>
@endsection


@section('custom-script')
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBoLQyif0tfik3UOs6p5ftz5V6Ny4X7vlg&v=3.exp&sensor=false" type="text/javascript"></script>
    <script>
        // if HTML DOM Element that contains the map is found...
        if (document.getElementById('map-canvas')){

            // Coordinates to center the map
            var myLatlng = new google.maps.LatLng(-6.9114574,107.6075389);

            // Other options for the map, pretty much selfexplanatory
            var mapOptions = {
                zoom: 15,
                center: myLatlng,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };

            // Attach a map to the DOM Element, with the defined settings
            var map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);

        }
    </script>
    <script>

        // quick search regex
        var qsRegex;

        // init Isotope
        var $grid = $('.result-search').isotope({
            itemSelector: '.results',
            layoutMode: 'fitRows',
            filter: function() {
                return qsRegex ? $(this).text().match( qsRegex ) : true;
            }
        });

        // use value of search field to filter
        var $quicksearch = $('.quicksearch').keyup( debounce( function() {
            qsRegex = new RegExp( $quicksearch.val(), 'gi' );
            $grid.isotope();
        }, 200 ) );

        // debounce so filtering doesn't happen every millisecond
        function debounce( fn, threshold ) {
            var timeout;
            return function debounced() {
                if ( timeout ) {
                    clearTimeout( timeout );
                }
                function delayed() {
                    fn();
                    timeout = null;
                }
                timeout = setTimeout( delayed, threshold || 100 );
            }
        }
    </script>
@endsection
