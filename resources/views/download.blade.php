@extends('layout.app')

@section('title')
    Taman Bandung
@endsection

@section('description')

@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>
                    Unduhan
                </h1>
                <div class="section"><div class="data-download"><table class="table table-striped"><thead><tr><th class="text-center">No</th><th>Berkas</th><th class="hidden-xs hidden-sm">Deskripsi</th><!-- <th class="hidden-xs hidden-sm">Diperbaharui</th> --><th class="text-center" width="100"><span class="fa fa-download"></span> Unduh</th></tr></thead><tbody><tr><td class="text-center">1</td><td>PERDA Kota Bandung No. 07 Th. 2013</td><td class="hidden-xs hidden-sm">tentang Penyediaan, Penyerahan dan Pengelolaan Prasaran, Sarana dan Utilitas Perumahan Dan Permukiman</td><!-- <td class="hidden-xs hidden-sm"><span id="moment">2016-10-25 08:07:55</span></td> --><td class="text-center"><a href="http://diskamtam.bandung.go.id/storage/app/uploads/public/580/ebd/b00/580ebdb00d81b057772177.pdf" download=""><span class="fa fa-download"></span> Unduh</a></td></tr><tr><td class="text-center">2</td><td>PERWAL Kota Bandung No. 428 Th. 2010</td><td class="hidden-xs hidden-sm">tentang Rician Tugas Pokok, Fungsi, Uraian Tugas dan Tata Kerja Dinas Pemakaman dan Pertamanan Kota Bandung;</td><!-- <td class="hidden-xs hidden-sm"><span id="moment">2016-10-25 07:58:05</span></td> --><td class="text-center"><a href="http://diskamtam.bandung.go.id/storage/app/uploads/public/580/ebc/258/580ebc2582172482182673.pdf" download=""><span class="fa fa-download"></span> Unduh</a></td></tr><tr><td class="text-center">3</td><td>PERDA Kota Bandung No. 04 Th. 2012</td><td class="hidden-xs hidden-sm">tentang Penyelenggaraan Reklame;</td><!-- <td class="hidden-xs hidden-sm"><span id="moment">2016-10-25 07:57:26</span></td> --><td class="text-center"><a href="http://diskamtam.bandung.go.id/storage/app/uploads/public/580/ebc/050/580ebc050b0fd630834099.pdf" download=""><span class="fa fa-download"></span> Unduh</a></td></tr><tr><td class="text-center">4</td><td>PERDA Kota Bandung No. 19 Th. 2011</td><td class="hidden-xs hidden-sm">tentang Ketentuan Pelayanan Pemakaman Umum &amp; Pengabuan Mayat &amp; Retribusi Pelayanan Pemakaman &amp; Pengabuan Mayat;</td><!-- <td class="hidden-xs hidden-sm"><span id="moment">2016-10-25 07:56:46</span></td> --><td class="text-center"><a href="http://diskamtam.bandung.go.id/storage/app/uploads/public/580/ebb/d76/580ebbd760ca1599021123.pdf" download=""><span class="fa fa-download"></span> Unduh</a></td></tr><tr><td class="text-center">5</td><td>PERDA Kota Bandung No. 07 Th. 2011</td><td class="hidden-xs hidden-sm">tentang Pengelolaan Ruang Terbuka Hijau;</td><!-- <td class="hidden-xs hidden-sm"><span id="moment">2016-10-25 07:56:11</span></td> --><td class="text-center"><a href="http://diskamtam.bandung.go.id/storage/app/uploads/public/580/ebb/b70/580ebbb7014dc981795742.pdf" download=""><span class="fa fa-download"></span> Unduh</a></td></tr><tr><td class="text-center">6</td><td>PERDA Kota Bandung No. 08 Th. 2011</td><td class="hidden-xs hidden-sm">tentang Perubahan PERDA Kota Bandung No. 9 Th. 2009 tentang RPJMD Kota Bandung Th. 2009-2014;</td><!-- <td class="hidden-xs hidden-sm"><span id="moment">2016-10-25 07:54:11</span></td> --><td class="text-center"><a href="http://diskamtam.bandung.go.id/storage/app/uploads/public/580/ebb/724/580ebb7245bdf364799910.pdf" download=""><span class="fa fa-download"></span> Unduh</a></td></tr><tr><td class="text-center">7</td><td>PERDA Kota Bandung No. 25 Th. 2009</td><td class="hidden-xs hidden-sm">tentang Hutan Kota;</td><!-- <td class="hidden-xs hidden-sm"><span id="moment">2016-10-25 07:53:21</span></td> --><td class="text-center"><a href="http://diskamtam.bandung.go.id/storage/app/uploads/public/580/ebb/0d2/580ebb0d2a00a727402067.pdf" download=""><span class="fa fa-download"></span> Unduh</a></td></tr><tr><td class="text-center">8</td><td>PERDA Kota Bandung No. 11 Th. 2008</td><td class="hidden-xs hidden-sm">tentang Retribusi Pengelolaan Kawasan Konservasi Taman Tegalega;</td><!-- <td class="hidden-xs hidden-sm"><span id="moment">2016-10-25 07:52:45</span></td> --><td class="text-center"><a href="http://diskamtam.bandung.go.id/storage/app/uploads/public/580/eba/eb8/580ebaeb84174149146923.pdf" download=""><span class="fa fa-download"></span> Unduh</a></td></tr><tr><td class="text-center">9</td><td>PERDA Kota Bandung No. 01 Th. 2008</td><td class="hidden-xs hidden-sm">tentang Pengelolaan Kawasan Konservasi Taman Tegalega;</td><!-- <td class="hidden-xs hidden-sm"><span id="moment">2016-10-25 07:52:07</span></td> --><td class="text-center"><a href="http://diskamtam.bandung.go.id/storage/app/uploads/public/580/eba/c53/580ebac53d578032881973.pdf" download=""><span class="fa fa-download"></span> Unduh</a></td></tr><tr><td class="text-center">10</td><td>PERDA Kota Bandung No. 08 Th. 2008</td><td class="hidden-xs hidden-sm">tentang Rencana Pembangunan Jangka Panjang Daerah (RPJPD) Th. 2005-2025;</td><!-- <td class="hidden-xs hidden-sm"><span id="moment">2016-10-25 07:50:58</span></td> --><td class="text-center"><a href="http://diskamtam.bandung.go.id/storage/app/uploads/public/580/eba/7fd/580eba7fd0880102880660.pdf" download=""><span class="fa fa-download"></span> Unduh</a></td></tr><tr><td class="text-center">11</td><td>PERDA Kota Bandung No. 07 Th. 2008</td><td class="hidden-xs hidden-sm">tentang Tahapan, Tata Cara Penyusunan, Pengendalian &amp; Evaluasi Pelaksanaan Rencana Pembangunan serta Musyawarah Perencanaan Pembangunan Daerah; Sebagaimana telah diubah dengan PERDA Kota Bandung No. 5 Th. 2009;</td><!-- <td class="hidden-xs hidden-sm"><span id="moment">2016-10-25 07:50:20</span></td> --><td class="text-center"><a href="http://diskamtam.bandung.go.id/storage/app/uploads/public/580/eba/590/580eba590ad98026598203.pdf" download=""><span class="fa fa-download"></span> Unduh</a></td></tr><tr><td class="text-center">12</td><td>PERDA Kota Bandung No. 13 Th. 2007</td><td class="hidden-xs hidden-sm">tentang Pembentukan &amp; Susunan Organisasi Dinas Daerah Kota Bandung, sebagaimana telah diubah dengan PERDA Kota Bandung No. 13 Th. 2009;</td><!-- <td class="hidden-xs hidden-sm"><span id="moment">2016-10-25 07:49:40</span></td> --><td class="text-center"><a href="http://diskamtam.bandung.go.id/storage/app/uploads/public/580/eba/315/580eba315cdba972317777.pdf" download=""><span class="fa fa-download"></span> Unduh</a></td></tr><tr><td class="text-center">13</td><td>PERDA Kota Bandung No. 03 Th. 2005</td><td class="hidden-xs hidden-sm">tentang Penyelenggaraan Ketertiban, Kebersihan, &amp; Keindahan sebagaimana telah diubah dengan PERDA Kota Bandung No. 11 Th. 2005;</td><!-- <td class="hidden-xs hidden-sm"><span id="moment">2016-10-25 07:48:16</span></td> --><td class="text-center"><a href="http://diskamtam.bandung.go.id/storage/app/uploads/public/580/eb9/4a7/580eb94a7836e281406714.pdf" download=""><span class="fa fa-download"></span> Unduh</a></td></tr><tr><td class="text-center">14</td><td>PERDA Kota Bandung No. 08 Th. 2007</td><td class="hidden-xs hidden-sm">tentang Urusan Pemerintah Daerah Kota Bandung;</td><!-- <td class="hidden-xs hidden-sm"><span id="moment">2016-10-25 07:48:01</span></td> --><td class="text-center"><a href="http://diskamtam.bandung.go.id/storage/app/uploads/public/580/eb9/a47/580eb9a47c43e031369781.pdf" download=""><span class="fa fa-download"></span> Unduh</a></td></tr><tr><td class="text-center">15</td><td>PERDA Kota Bandung No. 18 Th. 2011</td><td class="hidden-xs hidden-sm">tentang Rencana Tata Ruang Wilayah (RTRW) Kota Bandung Th. 2011-2031;</td><!-- <td class="hidden-xs hidden-sm"><span id="moment">2016-10-25 07:45:08</span></td> --><td class="text-center"><a href="http://diskamtam.bandung.go.id/storage/app/uploads/public/580/eb9/228/580eb922874dd732354042.pdf" download=""><span class="fa fa-download"></span> Unduh</a></td></tr><tr><td class="text-center">16</td><td>PERDA Kota Bandung No. 02 Th. 2001</td><td class="hidden-xs hidden-sm">tentang Kewenangan Daerah Kota Bandung sebagai Daerah Otonom;</td><!-- <td class="hidden-xs hidden-sm"><span id="moment">2016-10-25 07:44:12</span></td> --><td class="text-center"><a href="http://diskamtam.bandung.go.id/storage/app/uploads/public/580/eb8/e6a/580eb8e6a4b8d977354389.pdf" download=""><span class="fa fa-download"></span> Unduh</a></td></tr><tr><td class="text-center">17</td><td>PERMENDAGRI No. 54 Th. 2010</td><td class="hidden-xs hidden-sm">tentang Pelaksanaan PP No. 08 Th. 2008 tentang Tahapan, Tata Cara Penyusunan, Pengendalian &amp; Evaluasi Pelaksanaan Rencana Pembangunan Daerah;</td><!-- <td class="hidden-xs hidden-sm"><span id="moment">2016-10-25 07:43:08</span></td> --><td class="text-center"><a href="http://diskamtam.bandung.go.id/storage/app/uploads/public/580/eb8/a87/580eb8a87ed2d681502869.pdf" download=""><span class="fa fa-download"></span> Unduh</a></td></tr><tr><td class="text-center">18</td><td>PP No. 08 Th. 2008</td><td class="hidden-xs hidden-sm">tentang Tahapan, Tata Cara Penyusunan, Pengendalian &amp; Evaluasi Pelaksanaan Rencana Pembangunan Daerah;</td><!-- <td class="hidden-xs hidden-sm"><span id="moment">2016-10-25 07:42:07</span></td> --><td class="text-center"><a href="http://diskamtam.bandung.go.id/storage/app/uploads/public/580/eb8/6d5/580eb86d5f47f536031528.pdf" download=""><span class="fa fa-download"></span> Unduh</a></td></tr><tr><td class="text-center">19</td><td>PP No. 63 Th. 2002</td><td class="hidden-xs hidden-sm">tentang Hutan Kota;</td><!-- <td class="hidden-xs hidden-sm"><span id="moment">2016-10-25 07:41:37</span></td> --><td class="text-center"><a href="http://diskamtam.bandung.go.id/storage/app/uploads/public/580/eb8/508/580eb85085c1b696795813.pdf" download=""><span class="fa fa-download"></span> Unduh</a></td></tr><tr><td class="text-center">20</td><td>PP No. 06 Th. 1995</td><td class="hidden-xs hidden-sm">tentang Perlindungan Tanaman;</td><!-- <td class="hidden-xs hidden-sm"><span id="moment">2016-10-25 07:41:07</span></td> --><td class="text-center"><a href="http://diskamtam.bandung.go.id/storage/app/uploads/public/580/eb8/32e/580eb832e91d3362928446.pdf" download=""><span class="fa fa-download"></span> Unduh</a></td></tr></tbody></table></div></div>
            </div>
        </div>
    </div>
@endsection


@section('custom-script')
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBoLQyif0tfik3UOs6p5ftz5V6Ny4X7vlg&v=3.exp&sensor=false" type="text/javascript"></script>
    <script>
        // if HTML DOM Element that contains the map is found...
        if (document.getElementById('map-canvas')){

            // Coordinates to center the map
            var myLatlng = new google.maps.LatLng(-6.9114574,107.6075389);

            // Other options for the map, pretty much selfexplanatory
            var mapOptions = {
                zoom: 15,
                center: myLatlng,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };

            // Attach a map to the DOM Element, with the defined settings
            var map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);

        }
    </script>
    <script>

        // quick search regex
        var qsRegex;

        // init Isotope
        var $grid = $('.result-search').isotope({
            itemSelector: '.results',
            layoutMode: 'fitRows',
            filter: function() {
                return qsRegex ? $(this).text().match( qsRegex ) : true;
            }
        });

        // use value of search field to filter
        var $quicksearch = $('.quicksearch').keyup( debounce( function() {
            qsRegex = new RegExp( $quicksearch.val(), 'gi' );
            $grid.isotope();
        }, 200 ) );

        // debounce so filtering doesn't happen every millisecond
        function debounce( fn, threshold ) {
            var timeout;
            return function debounced() {
                if ( timeout ) {
                    clearTimeout( timeout );
                }
                function delayed() {
                    fn();
                    timeout = null;
                }
                timeout = setTimeout( delayed, threshold || 100 );
            }
        }
    </script>
@endsection
