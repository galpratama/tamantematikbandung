@extends('layout.app')

@section('title')
    Taman Bandung
@endsection

@section('description')

@endsection

@section('content')
    <div id="map-canvas"></div>
    <div class="row">
        <div class="detail-container col-md-4 col-xs-12">
            <div id="myCarousel" class="carousel slide carousel-park" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                    <li data-target="#myCarousel" data-slide-to="1"></li>
                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner">
                    <div class="item active">
                        <img src="https://i.ytimg.com/vi/vXjd33qT-BM/maxresdefault.jpg" alt="Chicago" style="width: 100%;">
                    </div>

                    <div class="item">
                        <img src="https://aleut.files.wordpress.com/2015/02/dscn3527.jpg" alt="New york" style="width: 100%;">
                    </div>
                </div>

                <!-- Left and right controls -->
                <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#myCarousel" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
            <div class="detail">
                <h3>
                    <strong>{{ $place['name'] }}</strong>
                    <br>
                    <small>{{ $place['types'] }} &middot; {{ $place['districts'] }}, {{ $place['subdistricts'] }}</small>
                </h3>
                <p>
                    <strong>Deskripsi</strong> <br>
                    Taman ini sudah sejak lama berdiri, dan sering digunakan untuk berolahraga seperti bersepeda. Namun dengan sedikit renovasi dan ditambahnya fasilitas, taman ini semakin ramai dikunjungi, entah untuk bersantai, berolahraga maupun bermain.
                </p>
                <p>
                    <strong>Koordinat</strong> <br>
                    {{ $place['lat'] }}, {{ $place['lng'] }}
                </p>
                <p>
                    <strong>Fasilitas</strong> <br>
                    Air mancur kolam badak putih, Lampu taman warna-warni dimalam hari, Bunga-bunga dan tanaman kecil aneka warna, Prisma untuk menggembok cinta, Wi-fi
                </p>
                <div class="row">
                    <div class="col-md-7">
                        <a href="{{ url('login') }}" class="btn btn-block btn-danger">BOOKING TAMAN</a>
                    </div>
                    <div class="col-md-5" style="margin-left: 0; padding-left: 0">
                        <a href="#" class="btn btn-block btn-primary"
                           onclick="window.open('https://www.google.com/maps/dir//{{ $place['lat'] }},{{ $place['lng'] }}','width=600,height=600'); return false;">PETUNJUK ARAH</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('custom-script')
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBoLQyif0tfik3UOs6p5ftz5V6Ny4X7vlg&v=3.exp&sensor=false" type="text/javascript"></script>
    <script>
        // if HTML DOM Element that contains the map is found...
        if (document.getElementById('map-canvas')){

            // Coordinates to center the map
            var myLatlng = new google.maps.LatLng({{ $place['lat'] }},{{ $place['lng'] }});
            var myLatlngCenter = new google.maps.LatLng({{ $place['lat']}},{{ $place['lng'] - 0.005 }});

            // Attach a map to the DOM Element, with the defined settings
            var map = new google.maps.Map(document.getElementById("map-canvas"), {
                zoom: 16,
                center: myLatlngCenter,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });

            var contentString = '<div id="content">'+
                '<div id="siteNotice">'+
                '{{ $place['location'] }}'+
                '<br>' +
                '{{ $place['types'] }} &middot; {{ $place['districts'] }}, {{ $place['subdistricts'] }}'+
                '</div>';

            var infowindow = new google.maps.InfoWindow({
                content: contentString,
                maxWidth: 300
            });

            var marker = new google.maps.Marker({
                position: myLatlng,
                map: map,
                title: 'Hello World!',
                icon: {
                    size: new google.maps.Size(20, 32),
                    // The origin for this image is 0,0.
                    origin: new google.maps.Point(0,0),
                    // The anchor for this image is the base of the flagpole at 0,32.
                    anchor: new google.maps.Point(0, 32)
                }
            });

            infowindow.open(map, marker);

            marker.addListener('click', function() {
                infowindow.open(map, marker);
                });

        }
    </script>
    <script>

        // quick search regex
        var qsRegex;

        // init Isotope
        var $grid = $('.result-search').isotope({
            itemSelector: '.results',
            layoutMode: 'fitRows',
            filter: function() {
                return qsRegex ? $(this).text().match( qsRegex ) : true;
            }
        });

        // use value of search field to filter
        var $quicksearch = $('.quicksearch').keyup( debounce( function() {
            qsRegex = new RegExp( $quicksearch.val(), 'gi' );
            $grid.isotope();
        }, 200 ) );

        // debounce so filtering doesn't happen every millisecond
        function debounce( fn, threshold ) {
            var timeout;
            return function debounced() {
                if ( timeout ) {
                    clearTimeout( timeout );
                }
                function delayed() {
                    fn();
                    timeout = null;
                }
                timeout = setTimeout( delayed, threshold || 100 );
            }
        }
    </script>
    <script>
        //function bsCarouselAnimHeight() {
        $('.carousel').carousel({
            interval: 5000
        }).on('slide.bs.carousel', function (e) {
            var nextH = $(e.relatedTarget).height();
            console.log(nextH)
            console.log( $(this).find('.active.item').parent() )
            $(this).find('.active.item').parent().animate({
                height: nextH
            }, 500);
        });
        //}
    </script>
@endsection
