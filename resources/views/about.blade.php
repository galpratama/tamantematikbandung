@extends('layout.app')

@section('title')
    Taman Bandung
@endsection

@section('description')

@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <br><br>
                <h1>
                    Tentang Dinas
                </h1>
                <h2>KATA PENGANTAR</h2>
                <div id="pengantar" class="section"><div class="testimonials"><div class="active item"><p>Assalamu’alaikum Wr. Wb.
                                <br>Salam Sejahtera buat kita semua.</p><p style="text-align: justify;"><span>Puji syukur kita panjatkan kehadirat Allah SWT (Tuhan Yang Maha Esa), karena atas rahmat dan ridho-Nya kita dapat menyelesaikan dan memulai publikasi wadah informasi ini, yang harapannya dapat dinikmati oleh semua pihak, baik dari kalangan Pemerintah, Swasta dan Masyarakat pada umumnya. Hal ini merupakan salah satu upaya dari Pemerintah Daerah Kota Bandung, melalui Dinas Pemakaman dan Pertamanan, untuk terus menerus meningkatkan kualitas pelayanan publik dalam rangka memulihkan kepercayaan masyarakat kepada pemerintah yang ber-implikasi politis, yaitu semakin kuatnya legitimasi pemerintah dari masyarakat ataupun sebaliknya.</span></p><p style="text-align: justify;"><span>Salah satu tuntutan masyarakat di era reformasi dan otonomi daerah saat ini, adalah penyelenggaraan tugas-tugas pemerintahan yang efektif, efisien, bersih dan bebas KKN serta pelayanan prima. Oleh karena itu, kami dari Dinas Pemakaman dan Pertamanan bermaksud membuka sarana komunikasi dan informasi yang dapat diakses oleh siapa pun, kapan pun dan di mana pun, guna memberikan dan atau menerima informasi terutama mengenai pemakaman dan pertamanan yang merupakan tugas dan tanggung jawab kami sebagai bagian dari Pemerintah Daerah untuk wilayah Kota Bandung.</span></p></div></div></div>
                <h2>SEJARAH SINGKAT
                    <br><small>Dinas Perumahan dan Kawasan Pemukiman, Pertanahan, dan Pertamanan</small></h2><p style="text-align: justify;">Menurut Peraturan Daerah Kotamadya Tingkat II Bandung tentang Susunan Organisasi Dinas Kebersihan dan Keindahan Kota Kotamadya Daerah Tingkat II Bandung Dinas Pertamanan adalah merupakan Seksi dari Dinas Kebersihan dan Keindahan Kota, yang menyatu dengan gedung Dinas Kebersihan dan Keindahan Kota (DK3) JL. Badak Singa No 10.</p><p style="text-align: justify;">Seksi dari Dinas Kebersihan dan Keindahan Kota kemudian pindah ke jalan Tamansari no 51A menepati Gedung BAWS (Gedung Perusahaan Daerah Air Minum (PDAM)), dengan Peraturan Daerah Kotamadya Daerah Tingkat II Bandung No 18 Tahun 1983 Tanggal 17 Januari 1983 tentang Susunan Organisasi dan Tata Kerja Dinas Pertamanan Kotamadya Daerah Tingkat II Bandung.</p><p style="text-align: justify;">Pada tahun 1977 Seksi Pertamanan pindah ke JL. A.Yani No 295, yang bersatu dengan Dinas Kebersihan dan Keindahan Kota. Seksi Pertamanan yang bersatu dengan Dinas Kebersihan dan Keindahan tahun 1985 pindah ke Balaikota Gedung sebelah barat lantai 3 Kantor Asisten 2, dan pada tahun 1988 pindah kembali ke JL. Ambon No 1A menempati kantor Seksi Ireda Dipenda Kotamadya Bandung.</p><p style="text-align: justify;">Menurut Peraturan Daerah Kota Bandung No. 05 Tahun 2001 tanggal 07 Maret 2001 tentang Susunan Organisasi dan Tata Kerja Dinas Pertamanan Dan Pemakaman Kota Bandung, bergabunglah Dinas Pertamanan dan Pemakaman menempati kantor di JL. Ambon No.1A, JL.Seram No.2 , dan JL Pandu No.32.</p><p style="text-align: justify;">Berdasarkan Peraturan Daerah Kota Bandung No. 12 Tahun 2007 Tanggal 04 Desember 2007 tentang Pembentukan dan Susunan Organisasi Lembaga Teknis Daerah Kota Bandung yang menyatakan Sub Dinas Pemakaman menjadi Satuan Kerja Perangkat Daerah (SKPD) tersendiri yaitu Kantor Pengelolaan Pemakaman di JL Pandu No.32 sehingga Dinas Pertamanan sesuai dengan Peraturan Daerah Kota Bandung Nomor 13 Tahun 2007 tentang Pembentukan Organisasi Dinas Daerah Kota Bandung menjadi Dinas Pertamanan Kota Bandung berkedudukan di JL.Ambon No.1A dan JL Seram No.2.</p><p style="text-align: justify;">Pada tahun 2010 menurut Peraturan Daerah Kota Bandung No.13 Tahun 2009 tentang Peraturan Daerah Kota Bandung No. 13 Tahun 2007 tentang Pembentukan dan Perubahan Susunan Organisasi Dinas Daerah Kota Bandung bahwa Kantor Pengelolaan Pemakaman bergabung kembali dengan Dinas Pertamanan sehingga namen klaturnya menjadi Dinas Pemakaman dan Pertamanan.</p>
            </div>
        </div>
    </div>
@endsection


@section('custom-script')
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBoLQyif0tfik3UOs6p5ftz5V6Ny4X7vlg&v=3.exp&sensor=false" type="text/javascript"></script>
    <script>
        // if HTML DOM Element that contains the map is found...
        if (document.getElementById('map-canvas')){

            // Coordinates to center the map
            var myLatlng = new google.maps.LatLng(-6.9114574,107.6075389);

            // Other options for the map, pretty much selfexplanatory
            var mapOptions = {
                zoom: 15,
                center: myLatlng,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };

            // Attach a map to the DOM Element, with the defined settings
            var map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);

        }
    </script>
    <script>

        // quick search regex
        var qsRegex;

        // init Isotope
        var $grid = $('.result-search').isotope({
            itemSelector: '.results',
            layoutMode: 'fitRows',
            filter: function() {
                return qsRegex ? $(this).text().match( qsRegex ) : true;
            }
        });

        // use value of search field to filter
        var $quicksearch = $('.quicksearch').keyup( debounce( function() {
            qsRegex = new RegExp( $quicksearch.val(), 'gi' );
            $grid.isotope();
        }, 200 ) );

        // debounce so filtering doesn't happen every millisecond
        function debounce( fn, threshold ) {
            var timeout;
            return function debounced() {
                if ( timeout ) {
                    clearTimeout( timeout );
                }
                function delayed() {
                    fn();
                    timeout = null;
                }
                timeout = setTimeout( delayed, threshold || 100 );
            }
        }
    </script>
@endsection
