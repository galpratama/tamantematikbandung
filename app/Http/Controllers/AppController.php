<?php

namespace App\Http\Controllers;

use App\Helpers\API;
use App\Place;
use Illuminate\Http\Request;

class AppController extends Controller
{
    public function index()
    {
        $places = Place::all();

        return view('tematik')->with(compact('places'));
    }

    public function kota()
    {
        $places = Place::all();

        return view('kota')->with(compact('places'));
    }

    public function details(Request $request, $park_id)
    {
        $place = Place::find($park_id);
        return view('detail')->with(compact('place'));
    }

    public function about()
    {
        return view('about');
    }

    public function download()
    {
        return view('download');
    }

    public function contact()
    {
        return view('contact');
    }
}
