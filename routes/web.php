<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'AppController@index');

Auth::routes();

Route::get('/home', 'AppController@index')->name('home');
Route::get('/kota', 'AppController@kota')->name('kota');
Route::get('/unduh', 'AppController@download')->name('download');
Route::get('/details/{park_id}/{slug?}', 'AppController@details');
Route::get('/about', 'AppController@about')->name('about');
Route::get('/contact', 'AppController@contact')->name('contact');


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
